# Simple Bootstrap

**Make Simple Web Applications with
[Bootstrap](https://getbootstrap.com/)!**

This pack includes a `bootstrap_base` template and all the static files
needed to kick start your Bootstrap 4, Simple Web application.

## Included Blocks

Various elements can be overwritten in the templates, some require a
file, and some can accept text. These are all set in the dict passed to
reply_template/2 in simple web. If you declare your own, this will
clobber the defaults in the base so you may wish to re-include some
parts.

```
reply_template(bootstrap_base, data{html_attr:"lang=en", content:my_content_template}).
```

### Text Blocks

* **title**: You page title, eg: "My App!"
* **html_attrs**: Attributes for your html tag, eg: "lang=en"
* **body_attrs**: Attributes for your body tag, eg:
  'class="dark"'

### File Blocks

These are searched for relative to your template directory.

* **metas**: Meta tags for your head
* **styles**: Links to files to include in styles, don't forget this
  will clobber `<link href="/bootstrap/css/bootstrap.min.css"
  rel="stylesheet">`
* **navbar**: A block before the content for you to put your
  navbar.
* **content**: The main content block, wrapped in a container.
* **scripts**: A block to put your JavaScript links, not this will
  clobber all script tags for JQuery, Popper, and Bootstrap.

### Re-Including the defaults

Sometimes you just want to add a stylesheet but include the default one
too, same for scripts. You can include the defaults using a
dynamic_include. Example `styles`:

```html
{{ dynamic_include bootstrap("styles") }}
<link href="/static/css/my_styles.css rel="stylesheet">
```

Example `scripts`:

```html
{{ dynamic_include bootstrap("scripts") }}
<script></script>
```

Same pattern will work for `metas` too.


## Example Use

An example of how to use this package effectively can be found in
[Simple Web Examples: Bootstrap Templates](https://gitlab.com/PaulBrownMagic/simple_web_examples/tree/master/examples/bootstrap_templates).
