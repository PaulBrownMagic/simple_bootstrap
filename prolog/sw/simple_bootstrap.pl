:- module(simple_bootstrap,
    [
    ]
).

% Assert app dir so it can be run from anywhere
:- prolog_load_context(directory, Dir),
   asserta(user:file_search_path(bs_app, Dir)),
   asserta(user:file_search_path(bs_templates, bs_app(templates))),
   asserta(user:file_search_path(bs_static, bs_app(static))).

:- use_module(library(sw/simple_web)).

% Allow optional blocks
:- asserta(sw_config_handling:config(st_undefined, false)).

% Override render_reply for bootstrap base
:- asserta((sw_template_handling:render_reply(bootstrap_base, Data, Opts) :- bootstrap_reply(Data, Opts))).


:- use_module(library(st/st_expr)).
:- use_module(library(st/st_render)).

% Register template function
:- st_set_function(sw_templates, 1, sw_templates).
:- st_set_function(bootstrap, 1, bootstrap).

% Expand dynamic_included paths to user app templates
sw_templates(File, Path) :-
    absolute_file_name(sweb_templates(File), Path, []).

% Expand dynamic_include path to bootstrap templates
bootstrap(File, Path) :-
    absolute_file_name(bs_templates(File), Path, []).

%! bootstrap_reply(Data, Opts) is semidet.
%
%  Render the file with the local bootstrap base and user templates
bootstrap_reply(Data, Opts) :-
    current_output(Out),
    format("Content-type: text/html~n~n"),
    st_render_file(bs_templates(bootstrap_base), Data, Out, Opts).

% Setup static handling
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_files)).

http:location(bootstrap_static, "/bootstrap", []).

:- http_handler(bootstrap_static(.),
                http_reply_from_files(bs_static(.), []),
                [prefix]).
