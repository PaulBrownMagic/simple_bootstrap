name(simple_bootstrap).
version('0.0.2').
title('Bootstrap Extension to Simple Web').
author('Paul Brown', 'paul@paulbrownmagic.com').
home('https://gitlab.com/PaulBrownMagic/simple_bootstrap').
download('https://gitlab.com/PaulBrownMagic/simple_bootstrap/*').
requires('simple_web').
requires('simple_template').
